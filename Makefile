MAIN_PATH=cmd/bikepark-webserver/main.go
GOARCH:=amd64
GOOS:=linux
VERSION:=1.0.0
DOCKERNET:=bikepark
PRODUCT:=webserver
BUILDFLAGS:=""
BUILD:=$(shell date +%s)
GOENVS:=CGO_ENABLED=0

export GO111MODULE=on
export GOPRIVATE=gitlab.com/*
#export GOPRIVATE=gitlab.com/thebikepark/bikepark/bikepark

################################################################################
# LINT
################################################################################

.PHONY:
lint-ci: ## get linter for testing
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.43.0
	$(go env GOPATH)/bin/golangci-lint run

.PHONY: lint-deps
lint-deps: ## get linter for testing
	GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint@latest

.PHONY: lint
lint: lint-deps ## get linter for testing
	golangci-lint run

.PHONY: build-deps
build-deps:
	go mod tidy

.PHONY: build
build: build-deps ## build the base bikepark webserver application
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOENVS) go build -v -ldflags \
		"-X main.Version=$(VERSION) -X main.Build=$(BUILD) $(LDFLAGS)" \
		-tags $(TAGS) \
		$(BUILDFLAGS) -o $(PRODUCT) $(MAIN_PATH)

.PHONY:vendor
vendor:
	go mod vendor

.PHONY: docker-network
docker-network: ## spin up the local gym docker network so that all dockerized gym components can communicate
	if [ -z $$(docker network ls -q --filter 'name=$(DOCKERNET)') ]; then\
		docker network create $(DOCKERNET);\
	fi

.PHONY: docker-up
docker-up: vendor docker-network
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	up \
	-d \
	--force-recreate \
	--build \
	--remove-orphans \
	bikepark.web-server

.PHONY: docker-config
docker-config:
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	config