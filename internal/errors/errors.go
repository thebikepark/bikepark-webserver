package errors

import (
	"context"
	"log"
	"net/http"

	runtimev1 "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// APIErr human readable error message
type APIErr struct {
	Message string    `json:"message"`
	Code    int       `json:"code"`
	Details []Details `json:"details"`
}

// Details API error details
type Details struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

const serviceUnavailable = "Service required to execute this request is not available, please try again, and contact support if this problem persists."

// ErrorHandler description
func ErrorHandler(_ context.Context, _ *runtimev1.ServeMux, m runtimev1.Marshaler, rw http.ResponseWriter, _ *http.Request, e error) {
	rw.Header().Set("Content-Type", "application/json")
	apiE := toAPIErr(e)
	rw.WriteHeader(apiE.Code)
	if err := m.NewEncoder(rw).Encode(apiE); err != nil {
		log.Printf("failed to encode error: %v", err)
	}
}

// ErrorHandler description
func ErrorHandlerV2(_ context.Context, _ *runtime.ServeMux, m runtime.Marshaler, rw http.ResponseWriter, _ *http.Request, e error) {
	rw.Header().Set("Content-Type", "application/json")
	apiE := toAPIErr(e)
	rw.WriteHeader(apiE.Code)
	if err := m.NewEncoder(rw).Encode(apiE); err != nil {
		log.Printf("failed to encode error: %v", err)
	}
}

// toAPIErr converts a grpc error to a http error
func toAPIErr(e error) APIErr {
	APIErr := APIErr{
		Code:    500,
		Details: []Details{},
	}
	if s, ok := status.FromError(e); ok {
		// grpc connection is not available
		if s.Code() == codes.Unavailable {
			APIErr.Message = serviceUnavailable
			APIErr.Code = runtime.HTTPStatusFromCode(s.Code())
			return APIErr
		}
		details := make([]Details, 0)
		for _, detail := range s.Details() {
			switch t := detail.(type) {
			case *errdetails.BadRequest:
				for _, violation := range t.GetFieldViolations() {
					details = append(details, Details{
						Field:   violation.GetField(),
						Message: violation.GetDescription(),
					})
				}
			}
		}
		APIErr.Message = s.Message()
		APIErr.Code = runtime.HTTPStatusFromCode(s.Code())
		APIErr.Details = details
	}
	return APIErr
}
