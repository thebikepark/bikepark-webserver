package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/namsral/flag"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/encoding/protojson"

	bikepark_geo_v1 "gitlab.com/thebikepark/bikepark-geo-search/pkg"
	apiErr "gitlab.com/thebikepark/bikepark-webserver/internal/errors"
	auth "gitlab.com/thebikepark/bikepark/pkg/auth"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"gopkg.in/natefinch/lumberjack.v2"
)

type config struct {
	server struct {
		port string
		env  string
	}
	auth struct {
		jwtSigningKey string
		subject       string
		jwtTokenLife  time.Duration
	}
	grpc struct {
		address string
		port    string
	}
	geoGrpc struct {
		address string
		port    string
	}
}

func main() {
	c := config{}
	flag.StringVar(&c.server.port, "rest_port", ":8090", "api server port")
	flag.StringVar(&c.grpc.address, "bikepark_address", "localhost", "bikepark grpc address")
	flag.StringVar(&c.grpc.port, "bikepark_port", "50001", "bikepark grpc address")

	flag.StringVar(&c.geoGrpc.address, "bikepark_geo_address", "localhost", "geo search grpc address")
	flag.StringVar(&c.geoGrpc.port, "bikepark_geo_port", "50002", "geo search grpc address")

	flag.StringVar(&c.auth.jwtSigningKey, "jwt_signing_key", "/etc/pki/tls/CA/certs/auth-key.pem", "key for signing the JWTs")

	flag.StringVar(&c.server.env, "env", "production", "environment")

	flag.Parse()

	var logger *zap.Logger
	if c.server.env == "production" {
		w := zapcore.AddSync(&lumberjack.Logger{
			Filename:   "./log/file.log",
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     28, // days
		})
		core := zapcore.NewCore(
			zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
			w,
			zap.DebugLevel,
		)
		logger = zap.New(core)
	} else {
		var err error
		logger, err = zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
	}

	defer func() {
		if err := logger.Sync(); err != nil {
			log.Println("err: ", err.Error())
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mux := runtime.NewServeMux(
		runtime.WithErrorHandler(apiErr.ErrorHandlerV2),
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
			MarshalOptions: protojson.MarshalOptions{
				EmitUnpopulated: true,
				UseProtoNames:   false,
			},
		}),
	)

	key, err := auth.ReadPrivateKey(c.auth.jwtSigningKey)
	if err != nil {
		logger.Error("failed to read private auth key", zap.Error(err))
	}

	authInteceptor := auth.Interceptor(&key.PublicKey)
	if err != nil {
		logger.Error("failed to create auth interceptor", zap.Error(err))
	}
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(authInteceptor),
	}

	err = bikepark_data_v1.RegisterBikeParkHandlerFromEndpoint(ctx, mux, endpoint(c.grpc.address, c.grpc.port), opts)
	if err != nil {
		logger.Error("failed to register bike park handler", zap.Error(err))
	}

	err = bikepark_data_v1.RegisterBikeParkAuthHandlerFromEndpoint(ctx, mux, endpoint(c.grpc.address, c.grpc.port), []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())})
	if err != nil {
		logger.Error("failed to register bike park auth handler", zap.Error(err))
	}

	logger.Info(fmt.Sprintf("registred bikerpark grpc server on address %s:%s", c.grpc.address, c.grpc.port))

	err = bikepark_geo_v1.RegisterBikeParkGeoSearchHandlerFromEndpoint(ctx, mux, endpoint(c.geoGrpc.address, c.geoGrpc.port), []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())})
	if err != nil {
		logger.Error("failed to register bike park geo search handler", zap.Error(err))
	}

	logger.Info(fmt.Sprintf("registred bikerpark geo search grpc server on address %s:%s", c.geoGrpc.address, c.geoGrpc.port))

	srv := &http.Server{
		Addr:         c.server.port,
		Handler:      mux,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  180 * time.Second,
	}

	logger.Info(fmt.Sprintf("starting web server on port " + c.server.port))

	err = srv.ListenAndServe()
	if err != nil {
		logger.Error("failed to start web server", zap.Error(err))
	}
}

func endpoint(addr, port string) string {
	if port[0] != byte(':') {
		port = ":" + port
	}
	return addr + port
}
