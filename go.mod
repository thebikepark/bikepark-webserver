module gitlab.com/thebikepark/bikepark-webserver

go 1.17

require (
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.10.0
	github.com/namsral/flag v1.7.4-pre
	gitlab.com/thebikepark/bikepark v0.0.0-20221010152334-3e1fe7ee0b11
	gitlab.com/thebikepark/bikepark-geo-search v0.0.0-20221016124729-8227a0cb86ef
	go.uber.org/zap v1.22.0
	google.golang.org/genproto v0.0.0-20220429170224-98d788798c3e
	google.golang.org/grpc v1.46.0
	google.golang.org/protobuf v1.28.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.1.2 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/net v0.0.0-20220425223048-2871e0cb64e4 // indirect
	golang.org/x/sys v0.0.0-20220429233432-b5fbb4746d32 // indirect
	golang.org/x/text v0.3.7 // indirect
)

// replace gitlab.com/thebikepark/bikepark => /home/igor/projekti/bikepark-project/bikepark
